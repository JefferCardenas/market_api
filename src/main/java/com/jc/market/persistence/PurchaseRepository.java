package com.jc.market.persistence;

import com.jc.market.domain.dto.PurchaseDomain;
import com.jc.market.domain.repository.PurchaseDomainRepository;
import com.jc.market.persistence.crud.PurchaseCrudRepository;
import com.jc.market.persistence.entity.Product;
import com.jc.market.persistence.entity.Purchase;
import com.jc.market.persistence.entity.PurchasesProduct;
import com.jc.market.persistence.entity.PurchasesProductPK;
import com.jc.market.persistence.mapper.PurchaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository implements PurchaseDomainRepository {

    @Autowired
    private PurchaseCrudRepository purchaseCrudRepository;

    @Autowired
    private PurchaseMapper mapper;

    @Override
    public List<PurchaseDomain> getAll() {
        return mapper.toPurchasesDomain( (List<Purchase>) purchaseCrudRepository.findAll());
    }

    @Override
    public Optional<List<PurchaseDomain>> getPurchaseByClient(String clientId) {
        return purchaseCrudRepository.findByIdClient(clientId)
                .map(p -> mapper.toPurchasesDomain(p));
    }

    @Override
    public PurchaseDomain save(PurchaseDomain purchaseDomain) {

        Purchase purchase = mapper.fromPurchase(purchaseDomain);
        
        purchase.getPurchasesProducts().forEach(product -> product.setPurchase(purchase));

        return mapper.toPurchaseDomain(purchaseCrudRepository.save(purchase));
    }
}
