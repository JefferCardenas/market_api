package com.jc.market.persistence;

import com.jc.market.domain.dto.ProductDomain;
import com.jc.market.domain.repository.ProductDomainRepository;
import com.jc.market.persistence.crud.ProductCrudRepository;
import com.jc.market.persistence.entity.Product;
import com.jc.market.persistence.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository implements ProductDomainRepository {

    @Autowired
    private ProductCrudRepository productCrudRepository;
    @Autowired
    private ProductMapper mapper;

    @Override
    public List<ProductDomain> getAll() {

        return mapper.toProductsDomains((List<Product>) productCrudRepository.findAll());
    }

    @Override
    public Optional<List<ProductDomain>> getByCategory(int categoryId) {

        List<Product> products = productCrudRepository.findByIdCategoryOrderByNameAsc(categoryId);

        return Optional.of(mapper.toProductsDomains(products));
    }

    @Override
    public Optional<List<ProductDomain>> getScarseProducts(int amount) {

        Optional<List<Product>> products = productCrudRepository.findByAmountStockLessThanAndState(amount, true);

        return products.map(p -> mapper.toProductsDomains(p) );
    }

    @Override
    public Optional<ProductDomain> getProduct(int productId) {

        return productCrudRepository.findById(productId).map(p -> mapper.toProductDomain(p));
    }

    @Override
    public ProductDomain save(ProductDomain productDomain) {

        Product product = mapper.toProduct(productDomain);

        return mapper.toProductDomain(productCrudRepository.save(product));
    }

    @Override
    public void delete(int productId) {
        productCrudRepository.deleteById(productId);
    }
}
