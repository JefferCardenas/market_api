package com.jc.market.persistence.mapper;

import com.jc.market.domain.dto.PurchaseItemDomain;
import com.jc.market.persistence.entity.PurchasesProduct;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PurchaseItemMapper {

    @Mapping(source = "id.idProduct", target = "productId")
    @Mapping(source = "amount", target = "quantity")
    @Mapping(source = "total", target = "total")
    @Mapping(source = "state", target = "active")
    PurchaseItemDomain toPurchaseItemDomain(PurchasesProduct purchasesProduct);
    List<PurchaseItemDomain> toPurchaseItemDomains(List<PurchaseItemDomain> purchaseItemDomains);

    @InheritInverseConfiguration
    @Mapping(target = "purchase", ignore = true)
    @Mapping(target = "product", ignore = true)
    @Mapping(target = "id.idPurchase", ignore = true)
    PurchasesProduct fromPurchasesProduct(PurchaseItemDomain purchasesProduct);
}
