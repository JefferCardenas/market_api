package com.jc.market.persistence.mapper;

import com.jc.market.domain.dto.ProductDomain;
import com.jc.market.persistence.entity.Product;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CategoryMapper.class})
public interface ProductMapper {


    @Mapping(source = "idProduct", target = "productId")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "idCategory", target = "categoryId")
    @Mapping(source = "sellingPrice", target = "price")
    @Mapping(source = "amountStock", target = "stock")
    @Mapping(source = "state", target = "active")
    @Mapping(source = "category", target = "categoryDomain")
    ProductDomain toProductDomain(Product product);
    List<ProductDomain> toProductsDomains(List<Product> products);

    @InheritInverseConfiguration
    @Mapping(target = "barcode", ignore = true)
    Product toProduct(ProductDomain productDomain);
}
