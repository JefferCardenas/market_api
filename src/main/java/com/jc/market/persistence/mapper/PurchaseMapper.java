package com.jc.market.persistence.mapper;


import com.jc.market.domain.dto.PurchaseDomain;
import com.jc.market.persistence.entity.Purchase;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {PurchaseItemMapper.class})
public interface PurchaseMapper {

    @Mapping(source = "idPurchase", target = "purchaseId")
    @Mapping(source = "idClient", target = "clientId")
    @Mapping(source = "date", target = "date")
    @Mapping(source = "paymentMethod", target = "paymentMethod")
    @Mapping(source = "comment", target = "comment")
    @Mapping(source = "state", target = "state")
    @Mapping(source = "purchasesProducts", target = "items")
    PurchaseDomain toPurchaseDomain(Purchase purchase);
    List<PurchaseDomain> toPurchasesDomain(List<Purchase> purchaseDomains);

    @InheritInverseConfiguration
    @Mapping(target = "client", ignore = true)
    Purchase fromPurchase(PurchaseDomain purchaseDomain);
}
