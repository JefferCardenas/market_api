package com.jc.market.domain.repository;

import com.jc.market.domain.dto.PurchaseDomain;

import java.util.List;
import java.util.Optional;

public interface PurchaseDomainRepository {

    List<PurchaseDomain> getAll();

    Optional<List<PurchaseDomain>> getPurchaseByClient(String clientId);

    PurchaseDomain save(PurchaseDomain purchaseDomain);
}
