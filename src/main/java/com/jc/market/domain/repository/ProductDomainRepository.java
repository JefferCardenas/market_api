package com.jc.market.domain.repository;

import com.jc.market.domain.dto.ProductDomain;

import java.util.List;
import java.util.Optional;

public interface ProductDomainRepository {

    List<ProductDomain> getAll();
    Optional<List<ProductDomain>> getByCategory(int categoryId);
    Optional<List<ProductDomain>> getScarseProducts(int amount);
    Optional<ProductDomain> getProduct(int productId);
    ProductDomain save(ProductDomain productDomain);
    void delete(int productId);
}
