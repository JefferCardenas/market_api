package com.jc.market.domain.service;

import com.jc.market.domain.dto.PurchaseDomain;
import com.jc.market.domain.repository.PurchaseDomainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PurchaseService{

    @Autowired
    private PurchaseDomainRepository purchaseDomainRepository;

    public List<PurchaseDomain> getAll(){
        return purchaseDomainRepository.getAll();
    }

    public Optional<List<PurchaseDomain>> getPurchaseByClient(String clientId){
        return purchaseDomainRepository.getPurchaseByClient(clientId);
    }

    public PurchaseDomain save(PurchaseDomain purchaseDomain){
        return purchaseDomainRepository.save(purchaseDomain);
    }
}
