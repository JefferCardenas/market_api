package com.jc.market.domain.service;

import com.jc.market.domain.dto.ProductDomain;
import com.jc.market.domain.repository.ProductDomainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductDomainRepository productDomainRepository;

    public List<ProductDomain> getAll(){
        return productDomainRepository.getAll();
    }

    public Optional<ProductDomain> getProduct(Integer productId){
        return productDomainRepository.getProduct(productId);
    }

    public Optional<List<ProductDomain>> getByCategory(int categoryId){
        return productDomainRepository.getByCategory(categoryId);
    }

    public Optional<List<ProductDomain>> getScarseProducts(int amount){
        return productDomainRepository.getScarseProducts(amount);
    }

    public ProductDomain save(ProductDomain productDomain){
        return productDomainRepository.save(productDomain);
    }

    public boolean delete(int productId){

        return getProduct(productId).map(p -> {
            productDomainRepository.delete(productId);
            return true;
        }).orElse(false);
    }

}
