package com.jc.market.web.controller;

import com.jc.market.domain.dto.PurchaseDomain;
import com.jc.market.domain.service.PurchaseService;
import com.jc.market.persistence.entity.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;


@RestController
@RequestMapping(value = "/purchases")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @GetMapping()
    public ResponseEntity<List<PurchaseDomain>> getAllPurchase(){
        return new ResponseEntity<>(purchaseService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/client/{clientId}")
    public ResponseEntity<List<PurchaseDomain>> getPurchaseByClient(@PathVariable("clientId") String clientId){
        return purchaseService.getPurchaseByClient(clientId)
                .map(p -> new ResponseEntity<>(p, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping()
    public ResponseEntity<PurchaseDomain> save(@RequestBody PurchaseDomain purchaseDomain){

        return new ResponseEntity<>(purchaseService.save(purchaseDomain), HttpStatus.CREATED);
    }

}
